<?php

function signup_pay_paypal_ipn() {

  //ERROR CHECKING

  // Verify that the request came from Paypal, and not from some intrusion
  if (!simple_paypal_ipn_verify($_POST)) {

    // Verification failed
    return;
  }

  // If payment is not for the email address configured for the seller account,
  // throw it out and log it.
  if ($_POST['receiver_email'] != variable_get('signup_pay_email', SIGNUP_PAY_DEFAULT_EMAIL)) {
    watchdog('signup_pay', 'Payment email (%payment) is not for the email address configured (%configured)', array('%payment' => variable_get('signup_pay_email', SIGNUP_PAY_DEFAULT_EMAIL), '%configured' => $_POST['receiver_email']));
    return;
  }

  // Check to see if the transaction ID already exists.  If it does, throw
  // it out.
  $transaction_id = check_plain($_POST['txn_id']);
  $result = db_query("SELECT nid FROM {signup_pay} WHERE transaction_id = '%s'", $transaction_id);
  $row = db_fetch_object($result);
  if (isset($row->nid)) {
    watchdog('signup_pay', 'IPN duplicate for transaction ID %tid. Duplicate is discarded so that it does not appear as a duplicate payment.', array('%tid' => $transaction_id));
    return;
  }

  //PREPARE POST VARS FOR DB

  // Parse the custom field, and break it into its components
  $data = explode(':', $_POST['custom']);
  $custom = array();
  foreach ($data as $row) {
    list($key, $value) = explode('=', $row);
    $custom[$key] = $value;
  }

  $time_paid = check_plain(strtotime($_POST['payment_date']));
  $name = check_plain($_POST['first_name'] .' '. $_POST['last_name'] . ($_POST['payer_business_name'] ? ' ('. $_POST['payer_business_name'] .')' : ''));
  $gross_amount = check_plain((float)$_POST['mc_gross']);
  $net_amount   = check_plain((float)$_POST['mc_gross'] - (float)$_POST['mc_fee']);

  // If an anonymous user, use the address they entered into the form.  Otherwise,
  // use their email address they use for the PayPal login.
  if (!$custom['mail']) {
    $mail = check_plain($_POST['payer_email']);
  }
  else {
    $mail = $custom['mail'];
  }

  $currency = check_plain($_POST['mc_currency']);

  // INSERT TRANSACTION INTO DB

  // Build query to insert the transaction into the signup_pay table
  db_query("INSERT INTO {signup_pay} (id, uid, nid, method_id, time_paid, name, mail, currency, net_amount, amount, transaction_id)
    VALUES (0, %d, %d, %d, %d, '%s', '%s', '%s', %f, %f, '%s')",
    (int)$custom['uid'],
    (int)$custom['nid'],
    SIGNUP_PAY_METHOD_PAYPAL,
    $time_paid,
    $name,
    $mail,
    $currency,
    $net_amount,
    $gross_amount,
    $transaction_id);


  /**
   * NOTIFY REGISTRANT THAT THEY PAYMENT HAS BEEN RECEIVED 
   */

  /** Running this through the theme function was causing problems and I dind't
    understand why it was going through the theme() function anyway since it
    isn't using a template file.  But then, not sure I completely appreciate
    the theme() function's role.
  $body = theme('theme_signup_pay_receipt', $custom['uid'], $custom['nid'], $mail, 'mail');
  **/
  $key = 'signup_pay_email_receipt';
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  $params = array();
  $params['mail'] = $mail;
  $params['nid'] = $custom['nid'];

  // Mail the notification of payment received to the registrant
  $result = drupal_mail('signup_pay', $key, $mail, NULL, $params, $from);

  if ($result) {
    watchdog('signup_pay', 'Payment receipt sent to @mail: @body', array('@mail' => $result['to'], '@body' => $result['body']));
  }
  else {
    watchdog('signup_pay', 'Error when trying to send signup payment receipt to @mail', array('mail' => $mail));
  }

  /**
   * NOTIFY NODE ADMIN THAT A PAYMENT HAS BEEN RECEIVED
   */

  $key = 'signup_pay_email_notify_admin';
  $params = array();
  $admin_mail =  variable_get('signup_pay_notification_email_address', NULL);
  $params['mail'] = $mail;
  $params['nid'] = $custom['nid'];
  $result = drupal_mail('signup_pay', $key, $admin_mail, NULL, $params, $from);

  if ($result) {
    watchdog('signup_pay', 'Payment admin notification sent to @mail: @body', array('@mail' => $result['to'], '@body' => $result['body']));
  }
  else {
    watchdog('signup_pay', 'Error when trying to send signup payment admin notification to @mail', array('mail' => $mail));
  }

  /**
   * UPDATE THEIR SIGNUP STATUS 
   * (Dependent on signup_status module)
   */

  if (module_exists('signup_status')) {
    $signup_status_paid = variable_get(SIGNUP_PAY_PAID_STATUS_CODE, SIGNUP_PAY_DEFAULT_PAID_STATUS_CODE);
    
    // The signup_status_operations function no longer exists, so I am currently
    // writing an update right to the signup_status table.  This should be updated
    // to interface with an API when it is available.  The issue has been logged:
    // http://drupal.org/node/508294
    // the original code is preserved below.

    // Anonymous user
    if ((int)$custom['uid'] === 0) {
      $result = db_query("UPDATE {signup_log} SET status = %d WHERE nid = %d AND uid = 0 AND anon_mail = '%s'", $signup_status_paid, $custom['nid'], $custom['mail']);
      if (!$result) {
        watchdog('signup_pay', 'Error updating the status for anonymous user @mail', array('@mail', $custom['mail']));
      }
    }
    
    // Registred user
    else {
      $result = db_query("UPDATE {signup_log} SET status = %d WHERE nid = %d AND uid = '%d'", $signup_status_paid, $custom['nid'], $custom['uid']);
      if (!$result) {
        $user = user_load($custom['uid']);
        watchdog('signup_pay', 'Error updating the status for user @username', array('@username', $user->name));
      }
    }

    /*
    $user = (int)$custom['uid'] ? (int)$custom['uid'] : (int)$custom['uid'].":".$mail;
    $users = array($user);
    $new_code = variable_get(SIGNUP_PAY_PAID_STATUS_CODE, SIGNUP_PAY_DEFAULT_PAID_STATUS_CODE);
    if (function_exists(signup_status_operations)) {
      signup_status_operations($users, $custom['nid'], 'status_code', $new_code);
    } else { 
      watchdog('signup_status', 'Could not find function signup_status_operations().  Status was not updated upon successful payment.');
    }
    */
  }
}
