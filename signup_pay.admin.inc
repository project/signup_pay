<?php


/**
 * @file
 * Code for the Signup Pay admin interface and logic
 */

function signup_pay_settings() {
  $form['signup_pay_general'] = array(
    '#type'            => 'fieldset',
    '#title'           => t('General Configuration'),
  );
  $form['signup_pay_general']['signup_pay_enabled_default'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Enable payment on signup for all new signup enabled nodes by default'),
    '#default_value'  => variable_get('signup_pay_enabled_default', SIGNUP_PAY_DEFAULT_ENABLED),
  );
  $form['signup_pay_general']['signup_pay_enabled_methods'] = array(
    '#type'            => 'checkboxes',
    '#title'          => t('Accept payment using the following methods'),
    '#options'        => _signup_pay_get_methods(TRUE),
    '#default_value'   => variable_get('signup_pay_enabled_methods', array(SIGNUP_PAY_METHOD_PAYPAL => 1)),
  );
  $form['signup_pay_general']['signup_pay_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Paypal email address'),
    '#default_value' => variable_get('signup_pay_email', SIGNUP_PAY_DEFAULT_EMAIL),
    '#description' => t('If using PayPal to collect payments please specify your PayPal e-mail address.'),
  );
  $form['signup_pay_general']['signup_pay_notification_email_address'] = array(
    '#type' => 'textfield',
    '#title'          => t('Notification Email Address'),
    '#default_value'  => '',
    '#description'    => 'Enter the email address that should by notifiied with a signup pay transaction has been completed.',
    '#default_value'  => variable_get('signup_pay_notification_email_address', SIGNUP_PAY_DEFAULT_NOTIFICATION_EMAIL_ADDRESS),
  );
  $form['signup_pay_general']['signup_pay_amount'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Default amount to charge, per node'),
    '#default_value'  => variable_get('signup_pay_amount', SIGNUP_PAY_DEFAULT_AMOUNT),
    '#description' => t('Default amount to charge each user. This can be changed on individual nodes. Do not change this after the first person pays.'),
  );
  $form['signup_pay_general']['signup_pay_currency'] = array(
    '#type'           => 'select',
    '#title'          => t('Default Currency'),
    '#default_value'  => variable_get('signup_pay_currency', SIGNUP_PAY_DEFAULT_CURRENCY),
    '#options'        => _signup_pay_get_currency(),
  );
  $form['signup_pay_general']['signup_pay_deny_anon'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Deny Anonymous access to Paying for an event.'),
    '#default_value'  => variable_get('signup_pay_deny_anon', SIGNUP_PAY_DEFAULT_DENY_ANON),
  );
  
  //TODO stronger admin features per role for default price and weighting for selecting correct amount

  $roles = user_roles(variable_get('signup_pay_deny_anon', SIGNUP_PAY_DEFAULT_DENY_ANON), SIGNUP_PAY_PERM_SIGNUP);
  
  $role_description = array();
  $default_role_description = array();
  foreach ($roles as $key => $value) {
    $role_description[] = $key . " = " . $value;
    $default_role_description[] = $key;
  }

  // Dislay an error message if no role assigned the signup permission
  if (!count($role_description)) {
    drupal_set_message(t('You have NOT assigned any roles the "@signup_perm" permission. Please visit the !url page and enable signup for at least one role. Until you do so, this module will not function correctly.', array('@signup_perm' => SIGNUP_PAY_PERM_SIGNUP, '!url' => l('permissions', 'admin/user/access'))), 'error');
  }

  $form['signup_pay_general']['signup_pay_role_weight'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Role Weight'),
    '#default_value'  => variable_get('signup_pay_role_weight', implode(", ", $default_role_description)),
    '#description'    => t('KEY [@legend]<br>Order the roles to prioritize their use when determining the price to pay for signups. For now, enter the ID of the role in the order you would like them used.', array('@legend' => implode(", ", $role_description))),
  );
  
  $form['signup_pay_reciept_fieldset'] = array(
    '#type'            => 'fieldset',
    '#title'           => t('Template for receipt page'),
    '#collapsible'    => TRUE,
    '#collapsed'      => TRUE,
    '#description' => t('Template for receipt when displayed on the web site as a page. You can use any HTML, including the following tokens Date: @date, Payment method: @method, Amount: @amount, Name: @name, Email: @mail'),
  );
  $form['signup_pay_reciept_fieldset']['signup_pay_receipt_page'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Template for receipt page'),
    '#default_value'  => variable_get('signup_pay_receipt_page', SIGNUP_PAY_DEFAULT_RECEIPT),
  );
  $form['signup_pay_reciept_email_fieldset'] = array(
    '#type'            => 'fieldset',
    '#title'          => t('Template for receipt email'),
    '#collapsible'    => TRUE,
    '#collapsed'      => TRUE,
    '#description' => t('Template for receipt when emailed to the user. You can use any HTML, including the following tokens Date: @date, Payment method: @method, Amount: @amount, Name: @name, Email: @mail'),
  );
  $form['signup_pay_reciept_email_fieldset']['signup_pay_receipt_email'] = array(
    '#type'           => 'textarea',
    '#default_value'  => variable_get('signup_pay_receipt_email', SIGNUP_PAY_DEFAULT_RECEIPT),
  );

  if (module_exists('signup_status')) {
    $access = user_access(SIGNUP_PAY_PERM_MANAGE_CODES);
    if ($access) {
      $link = t('Note that you can also !here, such as "Paid".', array('!here' => l(t('insert your own status code'), 'admin/settings/signup_status/add')));
    }

    $options = array();
    foreach (signup_status_codes() as $cid => $code) {
      $options[$cid] = $code['name'];
    }

    $form['signup_pay_status_fieldset'] = array(
      '#type'           => 'fieldset',
      '#title'          => t('Signup Status Configuration'),
    );

    $form['signup_pay_status_fieldset'][SIGNUP_PAY_PAID_STATUS_CODE] = array(
      '#type'           => 'select',
      '#title'          => t('Paid Status Code'),
      '#options'        => $options,
      '#default_value'  => variable_get(SIGNUP_PAY_PAID_STATUS_CODE, SIGNUP_PAY_DEFAULT_PAID_STATUS_CODE),
      '#description'    => t('Select the status code which will be used when a payment transaction is completed successfully. ') . $link,
    );
  }

  return system_settings_form($form);
}


/**
 * Renders admin CRUD form from payments
*/
function signup_pay_edit($function, $mode) {

  $time_paid = format_date(time(), 'custom', 'Y-m-d H:i O');
  $amount = variable_get('signup_pay_amount', SIGNUP_PAY_DEFAULT_AMOUNT);

  // Get the payment information if edit or delete
  switch ($mode) {
    case 'edit':
    case 'delete':
      $id = (int)arg(4);
      if ($id) {
        $result = db_query('SELECT * FROM {signup_pay} WHERE id = %d', $id);
        $row = db_fetch_object($result);
        $time_paid = format_date($row->time_paid, 'custom', 'Y-m-d H:i O');
        $amount = $row->amount;
        $net_amount = $row->net_amount;
      }
  }

  // Build form
  switch ($mode) {

    case 'edit':
    case 'add':
    
      $form['id'] = array(
        '#type'          => 'hidden',
        '#value'         => $id,
        );
      $form['time_paid'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Date'),
        '#default_value' => $time_paid,
        '#size'          => 30,
        '#maxlength'     => 30,
        '#description'   => 'Format: Y-M-D H:M Z',
        );
      $form['uid'] = array(
        '#type'          => 'textfield',
        '#title'         => t('uid of payer'),
        '#size'          => 60,
        '#maxlength'     => 60,
        '#default_value' => $row->uid,
        '#description'   => t('The user ID of the person paying.'),
      );
      $form['nid'] = array(
        '#type'          => 'textfield',
        '#title'         => t('nid of event'),
        '#size'          => 60,
        '#maxlength'     => 60,
        '#default_value' => $row->nid,
        '#description'   => t('The node ID of the event to be paid.'),
      );
      $form['amount'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Gross amount'),
        '#size'          => 60,
        '#maxlength'     => 60,
        '#default_value' => $amount,
        '#description'   => t('Amount to be paid.'),
      );
      $form['net_amount'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Net amount'),
        '#size'          => 60,
        '#maxlength'     => 60,
        '#default_value' => $net_amount,
        '#description'   => t('Amount after any fees are deducted.'),
      );
      $methods = _signup_pay_get_methods();
      $methods[SIGNUP_PAY_METHOD_FREE] = t('Free attendance');
      $form['method'] = array(
        '#type'          => 'select',
        '#title'         => t('Method'),
        '#options'       => $methods,
        '#default_value' => $row->method_id,
        '#description'   => t('Method of payment for this transaction.'),
      );
      $form['currency_code'] = array(
        '#type'           => 'select',
        '#title'          => t('Currency'),
        '#default_value'  => $row->currency,
        '#options'        => _signup_pay_get_currency(),
        '#name'           => 'currency_code',
        );
      $form['name'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Name of payer'),
        '#size'          => 60,
        '#maxlength'     => 60,
        '#default_value' => $row->name,
        '#description'   => t('Name of person or business paying, as it appears on the invoice.'),
      );
      $form['mail'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Email address'),
        '#size'          => 60,
        '#maxlength'     => 60,
        '#default_value' => $row->mail,
        '#description'   => t('Email address of person or business paying.'),
      );
      $form['add'] = array(
        '#type'  => 'submit',
        '#value' => t('Save'),
      );
      $form['mode'] = array(
        '#type'  => 'value',
        '#value' => $mode,
      );
      
      break;

    case 'delete':

      $form['id'] = array(
        '#type' => 'hidden',
        '#value' => $id,
        );
      $form['delete'] = array(
        '#type'  => 'submit',
        '#value' => t('Delete payment record for ' . $row->name),
      );
      $form['mode'] = array(
        '#type'  => 'value',
        '#value' => $mode,
      );

      break;
  }

  return $form;
}


function signup_pay_edit_submit($form, &$form_state) {
  global $user;
  $mode = $form_state['values']['mode'];

  if (isset($form_state['values']['delete'])) {
    $mode = 'delete';
  }

  switch ($mode) {
    case 'add':
      db_query("INSERT INTO {signup_pay} (id, uid, nid, time_paid, method_id, currency, amount, net_amount, mail, name)
        VALUES (0, %d, %d, %d, %d, '%s', %f, %f, '%s', '%s')",
        $form_state['values']['uid'],
        $form_state['values']['nid'],
        strtotime($form_state['values']['time_paid']),
        $form_state['values']['method'],
        $form_state['values']['currency_code'],
        $form_state['values']['amount'],
        $form_state['values']['net_amount'],
        $form_state['values']['mail'],
        $form_state['values']['name']);

      drupal_set_message(t('The payment has been added.'));
      watchdog('signup_pay', t('User @user added a transaction', array('@user' => $user->name)));

      break;

    case 'edit':
      db_query("UPDATE {signup_pay}
        SET uid = %d, nid = %d, time_paid = %d, method_id = %d, currency = '%s', amount = %f, net_amount = %f, mail = '%s', name = '%s'
        WHERE id = %d",
        $form_state['values']['uid'],
        $form_state['values']['nid'],
        strtotime($form_state['values']['time_paid']),
        $form_state['values']['method'],
        $form_state['values']['currency_code'],
        $form_state['values']['amount'],
        $form_state['values']['net_amount'],
        $form_state['values']['mail'],
        $form_state['values']['name'],
        $form_state['values']['id']);

      drupal_set_message(t('The payment has been updated.'));
      watchdog('signup_pay', t('User @user updated transaction @id', array('@user' => $user->name, '@id' => $form_state['values']['id'])));
      
      break;

    case 'delete':
      $result = db_query("DELETE FROM {signup_pay} WHERE id = %d", $form_state['values']['id']);
      drupal_set_message(t('The payment has been deleted.'));
      watchdog('signup_pay', t('User @user deleted transaction @id', array('@user' => $user->name, '@id' => $form_state['values']['id'])));
      break;
  }

  drupal_goto('admin/content/signup_pay');
}

function signup_pay_admin() {
  $methods = _signup_pay_get_methods();
  $methods[SIGNUP_PAY_METHOD_FREE] = t('Free attendance');
  $rows = array();
  $header = array(
    array('data' => t('Date'),   'field' => 'time_paid', 'sort' => 'desc'),
    array('data' => t('User'),   'field' => 'uid'),
    array('data' => t('Event'),  'field' => 'nid'),
    array('data' => t('Method'), 'field' => 'method_id'),
    array('data' => t('Amount'), 'field' => 'amount'),
    array('data' => t('Name'),   'field' => 'invoice_name'),
    array('data' => t('Operations'), 'colspan' => '2'),
  );

  $sql = 'SELECT u.uid, u.name, p.nid, u.mail,
    p.id, p.name as invoice_name, p.currency, p.amount, p.time_paid, p.method_id,
    p.mail AS registration_mail
    FROM {users} u
    INNER JOIN {signup_pay} p USING(uid)' . tablesort_sql($header);
    
  $result = pager_query($sql, SIGNUP_PAY_PAGER, 0, NULL);
  
  while ($row = db_fetch_object($result)) {
    $node = node_load($row->nid);
    
    // Decide what goes into the 'User' column
    $name = theme('username', $row);
    if ($row->uid == 0) {
      $name .= " $row->registration_mail";
    }
    
    $rows[] = array(
      format_date($row->time_paid, 'custom', 'Y-m-d H:i'),
      $name,
      l(t($node->title), "node/$row->nid/signups/admin"),
      $methods[$row->method_id],
      _signup_pay_format_amount($row->amount, $row->currency),
      $row->invoice_name,
      l(t('Edit'), "admin/content/signup_pay/edit/$row->id"),
      l(t('Delete'), "admin/content/signup_pay/delete/$row->id"),
      );
  }

  $output = '<p>' . t("Below is a list of all those who have paid for their event. The 'Name' column refers to the name as it appears on the PayPal account which may not be the same name as the registrant.") . '</p>';
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, SIGNUP_PAY_PAGER, 0);
  return $output;
}
