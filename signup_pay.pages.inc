<?php


/**
 * @file
 * Code for the Signup Pay admin interface and logic
 */

function signup_pay_payment_form(&$form_state, $node) {
  global $user;
  
  $anon = $_SESSION['signup_pay_anon_mail'];
  
  if ($node->signup_pay_enabled && _signup_pay_check_registered($node->nid, $anon) && !_signup_pay_check_payment($node->nid, $anon)) {

    drupal_set_title(t('Paying %node_title signup fee', array('%node_title' => $node->title)));
    
    list($amount, $currency) = _signup_pay_calculate_amount($node, $user);
    
    $form = array();
    
    $form['nid'] = array('#type' => 'value', '#value' => $node->nid);
    $form['uid'] = array('#type' => 'value', '#value' => $user->uid);
    
    //$form['signup_anon_mail'] = array('#type' => 'value', '#value' => $_SESSION['signup_pay_anon_mail']);
    
    $form['signup_pay'] = array(
      '#type' => 'fieldset',
      //'#title' => t('Signup payment'),
    );
    $form['signup_pay']['payment'] = array(
      // no need to specify #type => 'markup' as that is the default value
      '#value' => '<div>' . t('<b>Sign up fee:</b> <em>!amount</em>', array('!amount' => _signup_pay_format_amount($amount, $currency))) . '</div>',
    );
    $form['signup_pay']['method'] = array(
      '#type'          => 'radios',
      '#title'         => t('Payment method'),
      '#options'       => _signup_pay_get_methods(),
      '#default_value' => SIGNUP_PAY_METHOD_PAYPAL,
      '#description'   => t('Select a method of payment. If you select Credit Card/Paypal, you will be redirected to the Paypal site where you can pay using your credit card or balance from Paypal.'),
      '#required' => TRUE,
      '#after_build'   => array('signup_pay_form_check'),
    );
    $form['signup_pay']['proceed'] = array('#type' => 'submit', '#value' => t('Proceed to payment'));

    // Add a redirect to our Paypal form
    $form['#redirect'] = 'signup_pay_do_payment';
    
    return $form;
  }
  else {
    return drupal_not_found();
  }
}


function signup_pay_view_receipt() {
  global $user;

  $nid = arg(1);

  if ($user->uid && _signup_pay_check_payment()) {
    $output = theme('signup_pay_receipt', $user->uid, $nid);
  }
  else {
    $output = t('No receipt available.');
  }

  print theme('page', $output);
}

function theme_signup_pay_receipt($uid, $nid, $mail, $mode = 'page') {
  $nid = intval($nid);
  $uid = intval($uid);

  if ($nid < 1) {
    watchdog('signup_pay', 'Error calling theme_signup_pay_receipt. $uid is %uid and should be greater than 1. Receipt email or page was not generated.', array('%uid' => $uid));
    return;
  }

  if ($mode == 'mail') {
    $receipt = variable_get('signup_pay_receipt_email', SIGNUP_PAY_DEFAULT_RECEIPT);
  }
  else {
    $receipt = variable_get('signup_pay_receipt_page', SIGNUP_PAY_DEFAULT_RECEIPT);
  }

  $result = db_query('SELECT p.mail, p.name, p.nid, p.currency, p.amount, p.time_paid, p.method_id, n.title
    FROM {signup_pay} p
    INNER JOIN {node} n USING(nid)
    WHERE p.mail = "%s"
    AND p.nid = %d', 
    $mail, $nid);

  while ($row = db_fetch_object($result)) {
    $node = node_load($row->nid);
    $output = t($receipt, array(
      '@mail'   => $row->mail,
      '@title'  => $node->title,
      '@name'   => $row->name,
      '@amount' => _signup_pay_format_amount($row->amount, $row->currency),
      '@method' => $methods[$row->method_id],
      '@date'   => format_date($row->time_paid, 'custom', 'Y-m-d'),
    ));
  }
  return $output;
}

function signup_pay_thanks() {
  print theme('page', t('Thank you for registering.'));
}
