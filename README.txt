
Copyright 2007-2008 http://2bits.com

Description
-----------
Integrates with the signup module, and allows payment for a node while registering
for it.

See project page for more details.

To Do:
------
- Instead of giving just a drupal_set_message() when a user has registered and not
  paid, we need to show the payment form.
- Need a better way to check whether anonymous users paid or not. Right now it uses
  the stored email from the session, which is transient.

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/userpoints

Author
------
Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.

